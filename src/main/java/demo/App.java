package demo;

import demo.biomodels.CuratedModelsResponse;
import demo.biomodels.ModelResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 * @author Mihai Glon\u021b mglont@ebi.ac.uk
 */
public class App {
    public static void main(String[] args) {
        final String modelId = "BIOMD0000000010";

        try (BioModelsConnectionService bioModelsService = new BioModelsConnectionService()) {
            getModelInformation(modelId, bioModelsService);
            getCuratedModels(bioModelsService);
        }
    }

    private static void getCuratedModels(BioModelsConnectionService service) {
        CuratedModelsResponse models = service.getCuratedModelSet();
        int matches = models.getMatches();
        System.out.println("curated model count: " + matches);
    }

    private static void getModelInformation(String modelId,
            BioModelsConnectionService bioModelsService) {
        final ModelResponse modelInfo = bioModelsService.getModel(modelId);
        System.out.println("response = " + modelInfo);
    }
}
